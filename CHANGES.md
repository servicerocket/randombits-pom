# POM

## 2015-11-26, 7
- WIKI-586 Confluence 5.9.1 compatibility

## 2015-06-08, 6
- WIKI-335 Remove duplicated sr-oss-release profile and add new profile for compat build.

## 2015-06-04, 5
- WIKI-331 Add new profile for compatibility build.

## 2014-10-21, 2
- WIKI-73 Add additional repositories and pluginRepositories
- WIKI-73 Preconfigure `com.atlassian.maven.plugins:maven-refapp-plugin plugin` for integration testing