POM
===

This project consists of a POM that contains settings that are likely to be 
useful to any ServiceRocket Randombits (fka CustomWare) OSS project that is building and 
releasing code with Maven 2/3.

Most projects will want to override some of these settings.

The list of items configured in this POM are:

 - organization
 - url (Should be overriden)
 - repositories
 - distributionManagement
 - pluginManagement
 - plugins
